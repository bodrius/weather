import moment from 'moment';

export const transformDateToWeekDays = (array) => {
  if (array) {
    const newArray = array
      .map((item) => ({
        ...item,
        weekDay: moment(item.dt_txt).format('dddd'),
      }))
      .filter(
        (value, index, array) =>
          array.findIndex((item) => item.weekDay === value.weekDay) === index,
      );
    return newArray;
  } else {
    return [];
  }
};
