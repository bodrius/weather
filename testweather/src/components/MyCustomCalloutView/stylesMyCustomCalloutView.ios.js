import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: 120,
    height: 50,
  },
  title: {
    fontWeight: '600',
    marginBottom: 10,
    fontSize: 13,
  },
  containerLoader: {
    width: 120,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
