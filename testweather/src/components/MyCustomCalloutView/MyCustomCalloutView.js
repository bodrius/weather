import React from 'react';
import {View, Text} from 'react-native';
import {useSelector} from 'react-redux';
import {styles} from './stylesMyCustomCalloutView';
import Loader from '../../components/Loader/Loader';

const MyCustomCalloutView = ({city, country, temp}) => {
  const correctTemp = Math.round(temp);

  return (
    <>
      {city ? (
        <View style={styles.container}>
          <Text
            numberOfLines={2}
            ellipsizeMode={'tail'}
            style={styles.title}>{`${city}, ${country}`}</Text>
          <Text>{`${correctTemp}, ℃`}</Text>
        </View>
      ) : (
        <View style={styles.containerLoader}>
          <Loader />
        </View>
      )}
    </>
  );
};

export default MyCustomCalloutView;
