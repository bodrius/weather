import React from 'react';
import {Button, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';

import {resetError} from '../../redux/actions/errors';
import {styles} from './stylesShowError';
const ShowError = ({errorMessage}) => {
  const dispatch = useDispatch();
  const handelPress = () => {
    dispatch(resetError());
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{errorMessage}</Text>
      <Button title="OK" onPress={handelPress} />
    </View>
  );
};

export default ShowError;
