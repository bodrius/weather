import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';

import {Constants} from '../../helpers/constants';
import {changeRouteName} from '../../redux/actions/routeName';
import {resetWeather} from '../../redux/actions/getFiveDaysWeather';
import {styles} from './stylesMainButton';

const MainButton = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const activeRouteName = useSelector((state) => state.activeRouteName);

  const goToHomeScreen = () => {
    navigation.navigate(Constants.homeScreen);
    dispatch(changeRouteName(Constants.homeScreen));
    dispatch(resetWeather());
  };

  const goToSearchScreen = () => {
    navigation.navigate(Constants.searchScreen);
    dispatch(changeRouteName(Constants.searchScreen));
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.7}
        style={[
          styles.button,
          {
            backgroundColor:
              activeRouteName === Constants.homeScreen ? '#696969' : '#00bfff',
          },
        ]}
        onPress={goToHomeScreen}>
        <Text style={styles.title}>MAP</Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.7}
        style={[
          styles.button,
          {
            backgroundColor:
              activeRouteName === Constants.searchScreen
                ? '#696969'
                : '#00bfff',
          },
        ]}
        onPress={goToSearchScreen}>
        <Text style={styles.title}>SEARCH</Text>
      </TouchableOpacity>
    </View>
  );
};

export default MainButton;
