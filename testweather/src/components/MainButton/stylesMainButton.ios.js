import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    position: 'absolute',
    bottom: 0,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '50%',
    height: 50,
    marginBottom: 20,
    borderRadius: 30,
  },
  title: {
    fontSize: 20,
    color: 'snow',
    fontWeight: '600',
  },
});
