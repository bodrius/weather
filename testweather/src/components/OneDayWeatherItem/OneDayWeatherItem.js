import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './stylesOneDayWeatherItem';

const OneDayWeatherItem = ({item}) => {
  const temp = Math.round(item.main.temp);

  return (
    <View style={styles.container}>
      <Text style={styles.styleText}>{item.weekDay}</Text>
      <Text style={styles.styleText}>{`${temp}, ℃`}</Text>
    </View>
  );
};

export default OneDayWeatherItem;
