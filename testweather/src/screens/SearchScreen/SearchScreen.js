import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  View,
  FlatList,
  Image,
  Keyboard,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useRoute} from '@react-navigation/native';

import MainButton from '../../components/MainButton/MainButton';
import {styles} from './stylesSearchScreen';
import {
  getFiveDaysWeather,
  resetWeather,
} from '../../redux/actions/getFiveDaysWeather';
import OneDayWeatherItem from '../../components/OneDayWeatherItem/OneDayWeatherItem';
import {transformDateToWeekDays} from '../../helpers/transformDateToWeekDays';
import {checkError} from '../../redux/selectors/checkError';
import ShowError from '../../components/ShowError/ShowError';

const SearchScreen = () => {
  const [value, setValue] = useState('');
  const dispatch = useDispatch();
  const {params} = useRoute();
  const fiveDaysWeather = useSelector((state) => state.fiveDaysWeather?.list);
  const error = useSelector((state) => checkError(state.errors));

  const onPress = () => {
    dispatch(getFiveDaysWeather(value));
    setValue('');
    Keyboard.dismiss();
  };

  useEffect(() => {
    if (params?.city) {
      setValue(params.city);
      dispatch(getFiveDaysWeather(params.city));
    }
    return () => {
      dispatch(resetWeather());
    };
  }, []);

  return (
    <SafeAreaView style={styles.mainContainer}>
      <View style={styles.container}>
        <TextInput
          placeholder={'Enter sity name ...'}
          style={styles.input}
          onChangeText={(text) => setValue(text)}
          value={value}
          clearButtonMode={'always'}
        />
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.searchButton}
          onPress={onPress}>
          <Image
            style={styles.imageSearch}
            source={require('../../assets/searchIcon3.png')}
          />
        </TouchableOpacity>
      </View>
      {error && <ShowError errorMessage={'Please enter a valid city name'} />}
      <View style={styles.containerListItem}>
        <FlatList
          data={transformDateToWeekDays(fiveDaysWeather)}
          keyExtractor={(item) => item.dt_txt}
          renderItem={({item}) => <OneDayWeatherItem item={item} />}
        />
      </View>
      <MainButton />
    </SafeAreaView>
  );
};

export default SearchScreen;
