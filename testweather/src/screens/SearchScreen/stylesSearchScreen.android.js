import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'snow',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 30,
    marginHorizontal: 15,
    marginTop: 25,
  },
  input: {
    borderColor: 'gray',
    borderWidth: 1,
    height: 50,
    width: '70%',
    paddingLeft: 15,
    fontSize: 20,
    fontWeight: '600',
    borderRadius: 7,
  },
  searchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 30,
    borderColor: '#0bff',
    borderWidth: 2,
    width: '20%',
    fontWeight: '600',
  },
  imageSearch: {
    width: 40,
    height: 40,
  },
  containerListItem: {
    marginBottom: 187,
  },
});
