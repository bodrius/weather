import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  locationText: {
    fontWeight: '700',
    marginTop: 70,
    fontSize: 25,
    textAlign: 'center',
    color: '#1e90ff',
  },
});
