import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import MapView, {Marker, Callout, PROVIDER_GOOGLE} from 'react-native-maps';
import {useNavigation} from '@react-navigation/native';

import MainButton from '../../components/MainButton/MainButton';
import {styles} from './stylesHomeScreen';
import {getTodayWeather} from '../../redux/actions/getTodayWeather';
import MyCustomCalloutView from '../../components/MyCustomCalloutView/MyCustomCalloutView';
import Loader from '../../components/Loader/Loader';
import {Constants} from '../../helpers/constants';
import {changeRouteName} from '../../redux/actions/routeName';

const HomeScreen = () => {
  const [region, setRegion] = useState({
    latitude: 50.4302,
    longitude: 30.5198,
  });
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const city = useSelector((state) => state.todayWeather?.name);
  const country = useSelector((state) => state.todayWeather?.sys?.country);
  const temp = useSelector((state) => state.todayWeather?.main?.temp);

  useEffect(() => {
    dispatch(getTodayWeather(region));
  }, []);

  const marker = (ref) => {
    ref?.hideCallout();
  };

  const onPressMap = (event) => {
    setRegion({
      latitude: event.nativeEvent.coordinate.latitude,
      longitude: event.nativeEvent.coordinate.longitude,
    });
    dispatch(
      getTodayWeather({
        latitude: event.nativeEvent.coordinate.latitude,
        longitude: event.nativeEvent.coordinate.longitude,
      }),
    );
  };

  const goToSearchScreen = () => {
    navigation.navigate(Constants.searchScreen, {city});
    dispatch(changeRouteName(Constants.searchScreen));
  };

  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={{...StyleSheet.absoluteFillObject}}
        initialRegion={{
          latitude: 50.4302,
          longitude: 30.5198,
          latitudeDelta: 4.5,
          longitudeDelta: 4.5,
        }}
        onLongPress={(event) => onPressMap(event)}>
        <Marker
          ref={(ref) => {
            marker(ref);
          }}
          coordinate={region}
          image={require('../../assets/flag.png')}>
          <Callout onPress={goToSearchScreen}>
            <MyCustomCalloutView city={city} country={country} temp={temp} />
          </Callout>
        </Marker>
      </MapView>
      {city ? (
        <Text style={styles.locationText}>{`${city}, ${country}`}</Text>
      ) : (
        <Loader />
      )}
      <MainButton />
    </View>
  );
};

export default HomeScreen;
