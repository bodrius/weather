import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  locationText: {
    fontWeight: '900',
    marginTop: 70,
    fontSize: 20,
    textAlign: 'center',
    color: '#1e90ff',
  },
});
