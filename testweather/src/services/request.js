import axios from 'axios';
import {API_TOKEN} from 'react-native-dotenv';
const BASE_URL = 'https://api.openweathermap.org';

export const requests = {
  getTodayWeather: async (latitude, longitude) => {
    try {
      const weather = await axios.get(
        `${BASE_URL}/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${API_TOKEN}&units=metric`,
      );
      return weather.data;
    } catch (error) {
      return error;
    }
  },
  getFiveDaysWeather: async (cityName) => {
    try {
      const weather = await axios.get(
        `${BASE_URL}/data/2.5/forecast?q=${cityName}&appid=${API_TOKEN}&units=metric`,
      );
      return weather.data;
    } catch (error) {
      return error;
    }
  },
};
