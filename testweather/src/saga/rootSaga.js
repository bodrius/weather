import {all, fork} from 'redux-saga/effects';
import oneDaySagaWatcher from './getWeather';
import fiveDaysSagaWatcher from './getFiveDaysWeather';

export default function* rootSaga() {
  yield all([fork(oneDaySagaWatcher), fork(fiveDaysSagaWatcher)]);
}
