import {call, put, takeEvery} from 'redux-saga/effects';
import {putFiveDaysWeather} from '../redux/actions/getFiveDaysWeather';
import {setError, resetError} from '../redux/actions/errors';
import {requests} from '../services/request';
import {GET_FIVE_DAYS_WEATHER} from '../redux/types/getFiveDayWeather';

function* sagaWorkerFiveDays({payload}) {
  try {
    yield put(resetError());
    const data = yield call(requests.getFiveDaysWeather, payload);
    if (data.message) {
      yield put(setError(data.message));
    } else {
      yield put(putFiveDaysWeather(data));
    }
  } catch (error) {
    console.log('error', error);
  }
}

export default function* fiveDaysSagaWatcher() {
  yield takeEvery(GET_FIVE_DAYS_WEATHER, sagaWorkerFiveDays);
}
