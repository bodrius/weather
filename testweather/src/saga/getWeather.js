import {call, put, takeEvery} from 'redux-saga/effects';
import {putWeatherInfo} from '../redux/actions/getTodayWeather';
import {requests} from '../services/request';
import {GET_WEATHER} from '../redux/types/getTodayWeather';

function* sagaWorker({payload}) {
  const {latitude, longitude} = payload;
  try {
    const data = yield call(requests.getTodayWeather, latitude, longitude);
    yield put(putWeatherInfo(data));
  } catch (error) {
    console.log('error', error);
  }
}

export default function* oneDaySagaWatcher() {
  yield takeEvery(GET_WEATHER, sagaWorker);
}
