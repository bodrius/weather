import {PUT_FIVE_DAYS_WEATHER, RESET_WEATHER} from '../types/getFiveDayWeather';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case PUT_FIVE_DAYS_WEATHER:
      return action.payload;
    case RESET_WEATHER:
      return initialState;
    default:
      return state;
  }
};
