import {PUT_WEATHER_DATA} from '../types/getTodayWeather';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case PUT_WEATHER_DATA:
      return action.payload;
    default:
      return state;
  }
};
