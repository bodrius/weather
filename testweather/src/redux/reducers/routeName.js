import {CHANGE_ROUTE_NAME} from '../types/routeName';
import {Constants} from '../../helpers/constants';

const initialState = Constants.homeScreen;

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_ROUTE_NAME:
      return action.payload;
    default:
      return state;
  }
};
