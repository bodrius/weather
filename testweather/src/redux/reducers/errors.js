import {SET_ERROR, RESET_ERROR} from '../types/errors';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR:
      return {
        message: action.payload,
      };
    case RESET_ERROR:
      return initialState;
    default:
      return state;
  }
};
