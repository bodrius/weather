import {combineReducers} from 'redux';
import routeName from './routeName';
import todayWeather from './getTodayWeather';
import fiveDaysWeather from './getFiveDayWeather';
import errors from './errors';

const rootReducer = combineReducers({
  activeRouteName: routeName,
  todayWeather,
  fiveDaysWeather,
  errors,
});

export default rootReducer;
