import {GET_WEATHER, PUT_WEATHER_DATA} from '../types/getTodayWeather';

export const getTodayWeather = (coordinate) => ({
  type: GET_WEATHER,
  payload: coordinate,
});

export const putWeatherInfo = (weatherInfo) => ({
  type: PUT_WEATHER_DATA,
  payload: weatherInfo,
});
