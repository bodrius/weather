import {CHANGE_ROUTE_NAME} from '../types/routeName';

export const changeRouteName = (routeName) => ({
  type: CHANGE_ROUTE_NAME,
  payload: routeName,
});
