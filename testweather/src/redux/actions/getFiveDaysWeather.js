import {
  GET_FIVE_DAYS_WEATHER,
  PUT_FIVE_DAYS_WEATHER,
  RESET_WEATHER,
} from '../types/getFiveDayWeather';

export const getFiveDaysWeather = (cityName) => ({
  type: GET_FIVE_DAYS_WEATHER,
  payload: cityName,
});

export const putFiveDaysWeather = (weatherData) => ({
  type: PUT_FIVE_DAYS_WEATHER,
  payload: weatherData,
});

export const resetWeather = () => ({
  type: RESET_WEATHER,
});
